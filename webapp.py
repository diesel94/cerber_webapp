import time;
import os
from sqlite3 import dbapi2 as sqlite3
import sys
import threading
import subprocess
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash
from contextlib import closing

# configuration
DATABASE = '/home/node/webapp/tmp/cerber.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'
if len(sys.argv) == 4:
    SCHEDULER_PATH = sys.argv[1]
    MACHINEFILE_PATH = sys.argv[2]
    LOGS_PATH = sys.argv[3]
else:
    SCHEDULER_PATH = "./scheduler.py"
    MACHINEFILE_PATH = "./machinefile"
    LOGS_PATH = "./logs"

#globals
busy = False
processcount = 5

def callbackwhendone(data = [], *args):

    
    global processcount
    def runInThread(arglist = [], *args):
        with app.test_request_context():
            global busy
            from flask import g
            g.db = connect_db()
            print(command)
            starttime = time.clock()
            secondsStart = time.time()
            output = subprocess.check_output(arglist)
            endtime = time.clock()
            output = output.decode('ascii')
            f = open(LOGS_PATH+".csv",'a')
            f.write(arglist[2]+";"+ arglist[8]+";"+arglist[10]+";"+str(endtime-starttime)+";"+str(time.time()-secondsStart)+"\n")
            f.close();
            postcracked(arglist[8], output)
            crack()
            
    
    print("Creating a command")
    command = ["mpiexec", "-n", str(processcount), "-machinefile", MACHINEFILE_PATH, "python", SCHEDULER_PATH, "-H", str(data[1]), "-P", str(data[5])]
    if data[2]:
        command.append("-U")
    if data[3]:
        command.append("-N")
    if data[4]:
        command.append("-S")
    print(command)
    thread = threading.Thread(target=runInThread, args=(command,))
    thread.start()
    return thread


   
    
def crack():
    global busy
    
    print("Asking the db for a hash to crack")
    entry = gettocrack();
    if entry[1] is not None:
        busy = True
        callbackwhendone(entry)
        
    else:
        print("Db returned no hash to crack")
        busy = False
    
    
#database init
def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('cerber.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
    f = open("./logs.csv",'w')
    f.write("processcount;hashvalue;length;time(seconds)\n")
    f.close();

app = Flask(__name__)
app.config.from_object(__name__)    
        
@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()
       
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            return redirect(url_for('show_queue'))
    return render_template('login.html', error=error)       
def connect_db():
    return sqlite3.connect(app.config['DATABASE'])     
    

def gettocrack():
        cur = g.db.execute('select min(id), hashvalue, uppercase, digits, specials, explen from hashestocrack')
        entry = cur.fetchone()
        print(entry)
        return entry

def createmfile():
    f = open(MACHINEFILE_PATH,'w');
    cur = g.db.execute('select address from machines');
    rows = cur.fetchall()
    for row in rows:
        f.write(row[0]+"\n");
    f.close();
        
def postcracked(hashstring, crackedstring):
        print("posting received pass")
        g.db.execute('delete from hashestocrack where hashvalue = (?)',(hashstring,))
        g.db.execute('insert into cracked (hashvalue, crackedvalue) values (?,?)', (hashstring, crackedstring))
        g.db.commit()
        

@app.route('/deletemachine/<id>')
def delete_machine(id):
    if not session.get('logged_in'):
        abort(401)
    g.db.execute('delete from machines where id =(?)', (id,))
    g.db.commit()
    createmfile()
    return redirect(url_for('show_queue'))
    
@app.route('/delete/<id>')
def delete_entry(id):
    if not session.get('logged_in'):
        abort(401)
    g.db.execute('delete from hashestocrack where id =(?)', (id,))
    g.db.commit()
    return redirect(url_for('show_queue'))
    
@app.route('/')
def show_entries():
    cur = g.db.execute('select  hashvalue, crackedvalue from cracked')
    entries = [dict(hashvalue=row[0], crackedvalue=row[1]) for row in cur.fetchall()]
    return render_template('index.html', entries=entries)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('show_entries'))
    
@app.route('/changepc', methods=['POST'])
def changepc():
    global processcount
    if not session.get('logged_in'):
        return redirect(url_for('login'))
   
    processcount=request.form.get('processcount')
    print(processcount)
    return redirect(url_for('show_queue'))
    
@app.route('/test')
def test():
    crack();
    return redirect(url_for('show_queue'))
    
@app.route('/admin')
def show_queue():
    global processcount
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    pcount = processcount
    cur = g.db.execute('select id, hashvalue, uppercase, specials, digits, explen from hashestocrack')
    entries = [dict(id = row[0], hashvalue=row[1], uppercase=row[2], specials=row[3], digits=row[4], explen=row[5]) for row in cur.fetchall()]
    cur = g.db.execute('select id, address from machines')
    machines = [dict(id = row[0], address=row[1]) for row in cur.fetchall()]
    return render_template('admin_options.html', entries=entries, machines=machines, pcount=pcount)
       
@app.route('/addmachine', methods=['POST'])
def add_machine():
    
        
    address = request.form.get('address')

    try:
        g.db.execute('insert into machines (address) values (?)', (address,))
        g.db.commit()
        createmfile();
        flash('New entry was successfully posted')
        return redirect(url_for('show_queue')) 
    except sqlite3.IntegrityError:
        flash('This address was already added')
        return redirect(url_for('show_queue')) 

        
        
@app.route('/add', methods=['POST'])
def add_entry():
    global busy
    if request.form.get('fUC'):
        fUC = True
    else:
        fUC = False
        
    if request.form.get('fS'):
        fS = True
    else:
        fS = False
        
    if request.form.get('fD'):
        fD = True
    else:
        fD = False
        
    explen = request.form.get('explen')

    hashtopost = (request.form['hashvalue'], fUC, fS, fD, explen)
    cur = g.db.execute('select  hashvalue from cracked where hashvalue=(?)', (request.form['hashvalue'],))
    if cur.fetchone() is not None:
        flash('This hash was already cracked')
        print("Hash already cracked, not added")
        return redirect(url_for('show_entries'))
    else:
        try:
            g.db.execute('insert into hashestocrack (hashvalue, uppercase, specials, digits, explen) values (?, ?, ?, ?, ?)', hashtopost)
            g.db.commit()
            flash('New entry was successfully posted')
            print("Hash added to queue")
            if not busy:
                crack()
            return redirect(url_for('show_entries'))
            
        except sqlite3.IntegrityError:
            print("Hash already in queue, not added")
            flash('We are working on this one right now!')
            return redirect(url_for('show_entries'))


if __name__ == '__main__':
    app.run(host= '0.0.0.0')
