

drop table if exists hashestocrack;
create table hashestocrack (
  id integer primary key autoincrement,
  hashvalue varchar(32) unique not null,
  uppercase boolean default 0,
  specials boolean default 0,
  digits boolean default 0,
  explen integer default 8);
  
drop table if exists cracked;
create table cracked (
  hashvalue varchar(32) primary key not null,
  crackedvalue varchar(32) not null);
 
 drop table if exists machines;
 create table machines (
  id integer primary key autoincrement,
  address varchar(32) unique not null);
insert into cracked (hashvalue, crackedvalue) values ('c4049682126a3511443d8a0f9f61e3cc', 'wielkihipopotam');
insert into machines (address) values ('127.0.0.1');